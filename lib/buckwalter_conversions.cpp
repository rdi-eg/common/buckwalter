#include "rdi_buckwalter.hpp"

#include <rdi_stl_utils.hpp>

#include <algorithm>
#include <clocale>
#include <iostream>
#include <vector>

using namespace std;

namespace RDI
{

namespace
{

wstring
convert_buckwalter_to_arabic(const string& buckwalter, bool keep_tashkeel)
{
	wstring arabic;

	for (char c : buckwalter)
	{
		if (is_space(c))
		{
			arabic += space_to_wspace.at(c);
		}
		else if (keep_tashkeel ? is_arabic_letter_or_tashkeel(c) : is_arabic_letter(c))
		{
			arabic += buckwalter_to_arabic.at(c);
		}
	}

	return arabic;
}

bool
is_lam_alef(wchar_t c)
{
	// lam alef || lam alef mad || lam alef hamza above || lam alef hamza below
	return (c == L'\uFEFB' || c == L'\uFEF5' || c == L'\uFEF7' || c == L'\uFEF9');
}

string
convert_lam_alef(wchar_t c)
{
	// lam alef
	if(c == L'\uFEFB')
	{
		return "lA";
	}

	// lam alef mad
	if(c == L'\uFEF5')
	{
		return "l|";
	}

	// lam alef hamza above
	if(c == L'\uFEF7')
	{
		return "l>";
	}

	// lam alef hamza below
	if(c == L'\uFEF9')
	{
		return "l<";
	}

	return "";
}

string
convert_arabic_to_buckwalter(const wstring& arabic, bool keep_tashkeel,
							 const std::string& replace_unknown_with)
{
	string buckwalter;

	for (wchar_t c : arabic)
	{
		if(is_space(c))
		{
			buckwalter += wspace_to_space.at(c);
		}
		else if(is_arabic_letter(c))
		{
			buckwalter += arabic_to_buckwalter.at(c);
		}
		else if(is_lam_alef(c))
		{
			buckwalter += convert_lam_alef(c);
		}
		else if(keep_tashkeel and is_tashkeel(c))
		{
			buckwalter += arabic_to_buckwalter.at(c);
		}
		else if(!keep_tashkeel and is_tashkeel(c))
		{
			continue;
		}
		else
		{
			buckwalter += replace_unknown_with;
		}
	}

	return buckwalter;
}

} // nameless namespace

wstring
convert_buckwalter_to_arabic(const string& buckwalter)
{
	return convert_buckwalter_to_arabic(buckwalter, true);
}

wstring
convert_buckwalter_to_arabic_no_tashkeel(const string& buckwalter)
{
	return convert_buckwalter_to_arabic(buckwalter, false);
}

string
remove_tashkeel_from_buckwalter(const string& buckwalter)
{
	string output;

	for (char c : buckwalter)
	{
		if (is_space(c) or is_arabic_letter(c) or !is_tashkeel(c))
		{
			output += c;
		}
	}

	return output;
}
std::wstring remove_tashkeel_from_arabic(const std::wstring& arabic)
{
	std::wstring output;
	for(wchar_t c : arabic)
	{
		if (is_space(c) or is_arabic_letter(c) or !is_tashkeel(c))
		{
			output += c;
		}
	}
	return output;
}

wstring
convert_arabic_to_arabic_without_tashkeel(const wstring& arabic_with_tashkeel)
{
	wstring output;

	for (wchar_t c : arabic_with_tashkeel)
	{
		if (is_space(c) or is_arabic_letter(c))
		{
			output += c;
		}
	}

	return output;
}

string
convert_arabic_to_buckwalter(const wstring& arabic, const string& replace_unknown_with)
{
	return convert_arabic_to_buckwalter(arabic, true, replace_unknown_with);
}

string
convert_arabic_to_buckwalter_no_tashkeel(const wstring& arabic,
												const string& replace_unknown_with)
{
	return convert_arabic_to_buckwalter(arabic, false, replace_unknown_with);
}

bool is_unknown(wchar_t wc)
{
	if ( !is_space(wc) && !is_arabic_letter(wc) &&
	     !is_lam_alef(wc) && !is_tashkeel(wc) )
	{
		return true;
	}
	return false;
}

bool
is_tashkeel(wchar_t c)
{
	return within_container(c, _arabic_tashkeel);
}

bool
is_tashkeel(char c)
{
	return within_container(c, buckwalter_tashkeel);
}

bool
is_arabic_letter(wchar_t c)
{
	return within_container(c, _arabic_letters_without_tashkeel);
}

bool
is_arabic_letter(char c)
{
	return within_container(c, buckwalter_letters_without_tashkeel);
}

bool
is_arabic_letter_or_tashkeel(wchar_t c)
{
	return within_container(c, _arabic_letters_with_tashkeel);
}

bool
is_arabic_letter_or_tashkeel(char c)
{
	return within_container(c, buckwalter_letters_with_tashkeel);
}

bool
is_space(char c)
{
	return within_container(c, char_spaces);
}

bool
is_space(wchar_t c)
{
	return within_container(c, wspaces);
}

} // namespace RDI
