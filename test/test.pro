TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt


INCLUDEPATH +=  /opt/rdi/include  \
                ../lib

DEFINES += CODE_LOCATION="\\\"$$PWD/\\\""

CONFIG(release, debug|release) {
    LIBS += -L/opt/rdi/lib
}
CONFIG(debug, debug|release) {
    LIBS += -L/opt/rdi/lib_debug
}
LIBS += -Wl,--start-group  -lbuckwalter -Wl,--end-group

SOURCES += \
        main.cpp \
    tests.cpp
